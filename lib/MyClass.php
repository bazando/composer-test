<?php

namespace Tapbuy\ComposerTest\Lib;

class MyClass{
    
    protected $text = null;
    
    public function __construct($text)
    {
        $this->setText($text);
    }
    
    public function setText($text)
    {
        $this->text = $text;
    }
    
    public function getText()
    {
        return $this->text;
    }
    
    public function echo(){
        echo 'Text: '.$this->getText();
    }
    
}